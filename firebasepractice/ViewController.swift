//
//  ViewController.swift
//  firebasepractice
//
//  Created by Lovina on 27/08/18.
//  Copyright © 2018 Lovina. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
       // var ref: DatabaseReference!

     //   ref = Database.database().reference()
        var refArtists: DatabaseReference!
         refArtists = Database.database().reference().child("User");
        let key = refArtists.childByAutoId().key
        
        //creating artist with the given values
        let artist = ["id":key,
                      "artistName": "ajay",
                      "artistGenre": "hocky"
        ]
        
     
        refArtists.child(key).setValue(artist)
        
        
        self.checkUserNameAlreadyExist(newUserName: "ajay") { isExist in
            if isExist {
                print("Username exist")
            }
            else {
                print("create new user")
            }
        }
        
        
        
        //////Here recive data 
        
     
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func checkUserNameAlreadyExist(newUserName: String, completion: @escaping(Bool) -> Void) {
        
        let ref = Database.database().reference()
        ref.child("User").queryOrdered(byChild: "artistName").queryEqual(toValue: newUserName)
            .observeSingleEvent(of: .value, with: {(snapshot: DataSnapshot) in
                
                if snapshot.exists() {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}

