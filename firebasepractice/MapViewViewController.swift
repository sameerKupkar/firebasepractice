//
//  MapViewViewController.swift
//  firebasepractice
//
//  Created by Lovina on 29/08/18.
//  Copyright © 2018 Lovina. All rights reserved.
//

import UIKit
import  MapKit
import CoreLocation

class MapViewViewController:   UIViewController,MKMapViewDelegate,CLLocationManagerDelegate{
    var locationManager : CLLocationManager!
    @IBOutlet weak var MapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        MapView.delegate = self
        locationManager.delegate = self
        // Do any additional setup after loading the view.
    }
////
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
